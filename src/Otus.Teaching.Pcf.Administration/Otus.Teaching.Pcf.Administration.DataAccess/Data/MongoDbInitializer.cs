﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly MongoDbContext _mongoDataContext;

        public MongoDbInitializer(IMongoClient mongoClient, IMongoDatabase db)
        {
            _mongoDataContext = new MongoDbContext(mongoClient, db);
        }
        public MongoDbInitializer(MongoDbContext dataContext)
        {
            _mongoDataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _mongoDataContext.DropCollection<Role>();
            _mongoDataContext.AddRangeAsync(_mongoDataContext.Roles, FakeDataFactory.Roles);

            _mongoDataContext.DropCollection<Employee>();
            _mongoDataContext.AddRangeAsync(_mongoDataContext.Employees, FakeDataFactory.Employees);
        }
    }
}