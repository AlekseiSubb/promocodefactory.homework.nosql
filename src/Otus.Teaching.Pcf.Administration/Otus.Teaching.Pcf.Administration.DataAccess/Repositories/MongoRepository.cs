﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<TEntity> : IRepository<TEntity>
    where TEntity : BaseEntity
    {
        private readonly IMongoCollection<TEntity> _mongoCollection;
        private readonly MongoDbContext _mongoDataContext;

        public MongoRepository(IMongoClient mongoClient, IMongoDatabase db)
        {
            _mongoDataContext = new MongoDbContext(mongoClient,db);    
            _mongoCollection = _mongoDataContext.GetCollections<TEntity>();
        }

        public Task AddAsync(TEntity entity)
        {
            return _mongoCollection.InsertOneAsync(entity);
        }
        public Task UpdateAsync(TEntity entity)
        {
            return _mongoCollection.ReplaceOneAsync(item => item.Id == entity.Id, entity,
                new ReplaceOptions
                {
                    IsUpsert = true
                });
        }

        public Task DeleteAsync(TEntity entity)
        {
            return _mongoCollection.DeleteOneAsync(item => item.Id == entity.Id);
        }

        public Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return _mongoDataContext.GetAllAsync(_mongoCollection);
        }

        public Task<TEntity> GetByIdAsync(Guid id)
        {
            return _mongoDataContext.GetByIdAsync(_mongoCollection, id.ToString());
        }

        public Task<TEntity> GetFirstWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return _mongoDataContext.GetFirstWhereAsync(_mongoCollection, predicate);
        }

        public Task<IEnumerable<TEntity>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return _mongoDataContext.GetRangeByIdsAsync(_mongoCollection, ids.Select(x => x.ToString()).ToArray());
        }

        public Task<IEnumerable<TEntity>> GetWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return _mongoDataContext.GetWhereAsync(_mongoCollection, predicate);
        }
    }
}